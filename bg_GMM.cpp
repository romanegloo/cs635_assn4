#include "bg_GMM.h"

//initialize
BGD_GMM::BGD_GMM(int imgW, int imgH) :nWidth{imgW}, nHeight{imgH}
{
    // model properties:
    nK = 3;          // number of Gaussian distributions, usually 3-5.
                     // The best probable distribution determines the background
    fAlpha = 0.02f;  // To initialize the distribution parameters, at start the
                     // learning rate needs to start from 1 to the desired value
    fTsig = 3;     // standard deviation allowance for matching distribution.
    fSig0 = 5;       // initial standard deviation
    fTbgw = 0.85;     // if totalweight is greater than the threshold, the 
                     // given pixel is considered as a foreground

    // image properties: 
    // assume that image is in BGR format (3 channels)
    nChannels = 3;
    nWidthStep = nWidth * nChannels;
    nSize = nHeight * nWidth;

    // Gaussian Distribution per pixel
    mm = (gd*) malloc(nSize * nK * sizeof(gd));
    nGD = (uchar*) malloc(nSize);
    memset(nGD, 0, nSize);  // no distribution found at first
}

void BGD_GMM::bgdUpdatePixelGD(char* src, char* bg, char* bg_sub) {
    char* ptrPix = src;
    uchar* ptrGDused = nGD;
    //int debug = 290 * nWidth + 180;
    int debug = -1;

    // for each pixel
    for (int i = 0; i < nSize; ++i) 
    {
        bool bMatchFound = false;
        bool bBackground = false;
        int posPix = i * nK;
        int nModes = *(ptrGDused + i);
        float totalWeight = 0.0f;

        uchar b = src[i * nChannels];
        uchar g = src[i * nChannels + 1];
        uchar r = src[i * nChannels + 2];

        if (i == debug)
        {
            cout << "---------------------------------------" << endl;
            cout << "nModes: " << nModes << endl;
            cout << "pixel value:  [" << (int) r << ',' 
                << (int) g << ','
                << (int) b << ']' << endl;
        }
        // find the closest distribution
        for (int iGD = 0; iGD < nModes; ++iGD)
        {
            if (i == debug) cout << "====== comparing GDs, k=" << iGD << endl;
            int posPixK = posPix + iGD;
            float w = mm[posPixK].weight;

            if (!bMatchFound)  // matching distribution not found yet
            {
                float var = mm[posPixK].var;
                float muB = mm[posPixK].muB;
                float muG = mm[posPixK].muG;
                float muR = mm[posPixK].muR;

                float dB = muB - (uchar) src[i * nChannels];
                float dG = muG - (uchar) src[i * nChannels + 1];
                float dR = muR - (uchar) src[i * nChannels + 2];
                if (i == debug) 
                {
                    cout << "dB: " << dB << endl;
                    cout << "dG: " << dG << endl;
                    cout << "dR: " << dR << endl;
                }
                
                // pixel value difference
                float dist2 = (dB*dB + dG*dG + dR*dR);
                float comp = fTsig *fTsig * var;
                if(i == debug)
                    cout << "dist2: " << dist2 << ", "
                        << "comp: " << comp << endl;

                if ((totalWeight < fTbgw) && (dist2 < (fTsig * fTsig * var)))
                {
                    bBackground = true;
                }

                // pixel values are close to the distribution
                if (dist2 < (fTsig * fTsig * var))
                {

                    if (i== debug) cout << "MATCH FOUND" << endl;
                    // current pixel fits in this distribution
                    bMatchFound = true;

                    float rho = fAlpha / w;  //for computational simplicity

                    // update weight
                    // --------------------------------------------------------
                    // w_t = (1 - alpha) w_{t-1} + alpah (M_k), 
                    // where w_{t-1} is the prior weights and M_k is 1 for the
                    // model which matched and 0 for the remaining models.
                    w = (1 - fAlpha) * w + fAlpha;  // new weight
                    // TODO. need to normalize weights, will be done below

                    // update mean
                    // -------------------------------------------------------
                    // mu_t = (1 - rho) * mu_{t-1} + rho * X_t
                    //      = mu_{t-1} - (rho * difference)
                    // where rho is the Gaussian probability density function
                    mm[posPixK].muB = muB - rho * dB;
                    mm[posPixK].muG = muG - rho * dG;
                    mm[posPixK].muR = muR - rho * dR;

                    // update variance
                    // -------------------------------------------------------
                    // var = (1 - rho) * var + rho * (X_t - mu)'(X_t - mu), 
                    // Here, we assume the covariance matrix to be 'var * I'
                    // for computational reasons.
                    float var_ = var + rho * (dist2 - var);
                    // TODO. may need to limit the sigma value here

                    // sort Gaussian distributions by its weight in descending
                    // order, so that the first one can be easily used for the
                    // background
                    for (int i = iGD; i > 0; --i)
                    {
                        int pos = posPix + i;
                        if (w < mm[pos - 1].weight) break;
                        else
                        {
                            gd tmp = mm[pos];
                            mm[pos] = mm[pos - 1];
                            mm[pos - 1] = tmp;
                        }
                    }
                }
            }  
            else // found matching Gaussian distribution
            {
                // mu and sig remain the same,
                // but weight decreases
                w = (1 - fAlpha) * w;
            }
            totalWeight += w;
            mm[posPixK].weight = w;

            if (i == debug) 
            {
                cout << "var: " << mm[posPixK].var << endl;
                cout << "muB: " << mm[posPixK].muB << endl;
                cout << "muG: " << mm[posPixK].muG << endl;
                cout << "muR: " << mm[posPixK].muR << endl;
                cout << "weight: " << mm[posPixK].weight << endl;
            }
        }

        // normalize weights
        for (int iGD = 0; iGD < nModes; ++iGD)
        {
            mm[posPix + iGD].weight = mm[posPix + iGD].weight / totalWeight;
        }

        if (!bMatchFound)  // if there's no matching distribution at all
        {
            if (i == debug) cout << "creating new dist. nModes:" << nModes << endl;
            if (nModes == nK) { /* replace the last one */ }
            else
            {
                // create a new distribution
                nModes++;
            }
            int pos = posPix + nModes - 1;
            
            if (nModes == 1)  // very first distribution
                mm[pos].weight = 1;
            else 
                mm[pos].weight = fAlpha;

            // normalize weights
            for (int iGD = 0; iGD < nModes - 1; ++iGD)
            {
                mm[posPix + iGD].weight *= (1 - fAlpha);
            }

            mm[pos].muB = (uchar) src[i * nChannels];
            mm[pos].muG = (uchar) src[i * nChannels + 1];
            mm[pos].muR = (uchar) src[i * nChannels + 2];
            mm[pos].var = pow(fSig0, 2);

            // sort by weights -- from the end towards the highest weight
            // distribution
            for ( int iGD = nModes - 1; iGD > 0; --iGD)
            {
                int pos = posPix + iGD;
                if (fAlpha < mm[pos - 1].weight) break;
                else
                {
                    gd tmp = mm[pos];
                    mm[pos] = mm[pos - 1];
                    mm[pos - 1] = tmp;
                }
            }
        }
        *(ptrGDused+i) = nModes;

        // replace bg output pixels
        bg[i * nChannels] = mm[posPix].muB;
        bg[i * nChannels + 1] = mm[posPix].muG;
        bg[i * nChannels + 2] = mm[posPix].muR;

        if (bBackground) 
        {
            bg_sub[i * nChannels] = 0;
            bg_sub[i * nChannels + 1] = 0;
            bg_sub[i * nChannels + 2] = 0;
        }
        else 
        {
            bg_sub[i * nChannels] = 255;
            bg_sub[i * nChannels + 1] = 255;
            bg_sub[i * nChannels + 2] = 255;
        }


        if (i == debug)
        {
            cout << "where: " << i * nChannels << endl;
            cout << "replacing bg values to [" 
                << mm[posPix].muB << ','
                << mm[posPix].muG << ','
                << mm[posPix].muR << ']' << endl;
        }

    } // for each pixel
}
