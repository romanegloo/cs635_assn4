CC = g++
DEBUG = -g -std=c++11
CFLAGS = `pkg-config --cflags opencv` $(DEBUG)
LIBS = `pkg-config --libs opencv`

hw4 : hw4.cpp bg_GMM.cpp obj_tracking.cpp anl_crossline.cpp bg_GMM.h common.h anl_crossline.h
	$(CC) -o $@ hw4.cpp bg_GMM.cpp obj_tracking.cpp anl_crossline.cpp $(CFLAGS) $(LIBS)

