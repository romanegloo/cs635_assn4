#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
    Mat image;
    image = imread(argv[1], 1);

    if (!image.data) 
    {
        cout << "No image data" << endl;
        return -1;
    }
    namedWindow("Lena", WINDOW_AUTOSIZE);
    imshow("Display Image", image);

    waitKey(0);

    return 0;
}

