#ifndef __COMMON_H_INCLUDED__
#define __COMMON_H_INCLUDED__

using namespace std;

typedef unsigned char uchar;
typedef struct { int x, y; } vec2i;
typedef struct { double x, y; } vec2d;
typedef struct {
    vec2i pt1, pt2;        // bouding box from pt1 to pt2
} bb;
typedef struct {
    vec2i cent, pt1, pt2;
    float size;
    string status;
    bool pos_isLeft;               // position, in terms of the cross line
} movingObj;    

#endif
