#ifndef __ANLCROSSLINE_H_INCLUDED__
#define __ANLCROSSLINE_H_INCLUDED__
/*
 * Author: Jiho Noh (jiho@cs.uky.edu)
 *
 * another method to analyze pixels around the crossline
 */

using namespace cv;
using namespace std;

class Anl_CrossLine
{
    private:
    vec2i loi_1, loi_2;                     // cross line
    int imgW, imgH;                         // image width and height
    uchar* aoi;                             // mask of the area of interest
    vector<int> heatmap;                    // distribution of appearance
    float slopeInv;                         // slope of vehicle's direction
    vector<pair<vec2i, vec2i>> lanes;       // corners of each lane
    vec2i aoi_p1, aoi_p2, aoi_p3, aoi_p4;   // four corners of the area
    int gap;                                // width of the area
    vector<int> crossingStatus;             // crossing in, out, ...

    public:
    int totalCross;                        // count crossing
    Anl_CrossLine(vec2i, vec2i, int, int);  
        // initialize with crossline coordinates and the image size
    int drawAOI(IplImage*);                // draw the area on image
    void draw_line(vec2i pt1, vec2i pt2, IplImage* img);
    int updateHeatmap(char* data);         // update heatmap on loi
    void displayHeatmap();            // display heatmap in text
    int computeLanes();                    // analyze heatmap and find lanes
    void analyze(char*, char*, int);       // analyze bg_sub and determine cross

    private:
    int getAOI();                          // get the area of interest
    bool isPosRight(vec2i l1, vec2i l2, vec2i pt);
        // return true if the point is at right side of the line
};
#endif
