#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "common.h"
#include "obj_tracking.h"

using namespace std;

extern void draw_boundingBox( IplImage* img, bb obj, int color );

template<typename T>
float l2_distance(T pt1, T pt2)
{
    return sqrt(pow((pt1.x - pt2.x), 2) + pow((pt1.y - pt2.y), 2));
}


ObjTrack::ObjTrack(vec2i pt1, vec2i pt2)
{
    loi_1 = pt1, loi_2 = pt2;
    // parameters
    Tcost_new = 20.f;
    TminSize = 100;
    lastTrackId = 0;
    totalCross = 0;
    kClusters = -1;
}

void ObjTrack::assignDetections(map<int, bb> det)
{
    vector<int> unassigned;
    map<int, avgCent> matching;   // blob id --- set of matching tracks

    // for each detection
    for (auto it_d = det.begin(); it_d != det.end(); ++it_d)
    {
        bb b = it_d->second;
        // center of the box
        vec2d b_cent{(b.pt2.x + b.pt1.x) / 2., (b.pt2.y + b.pt1.y) / 2.};
        // matching distance depends on the size of the blob
        //float minCost = abs(b.pt1.x - b.pt2.x) ;
        float minCost = 60;

        // !! one blob can match multiple tracks. blobs that conisists of 
        // !! multiple objects occurs a lot.
        // for each tracks
        bool matchingFound = false;
        for (auto it_t = objs.begin(); it_t != objs.end(); ++it_t)
        {
            // compute cost: predicted centeroids ~ detection center
            vec2d t_cent = (*it_t).centroids.back();
            float dist = l2_distance(b_cent, t_cent);
            cout << "bid: " << it_d->first << " dist: " << dist
                << " minCost: " << minCost << endl;
            if (dist < minCost) {
                if (matching.find(it_d->first) == matching.end())
                {
                    matching[it_d->first].accX = t_cent.x;
                    matching[it_d->first].accY = t_cent.y;
                    matching[it_d->first].n = 1;
                } 
                else
                {
                    matching[it_d->first].accX += t_cent.x;
                    matching[it_d->first].accY += t_cent.y;
                    matching[it_d->first].n += 1;
                }
                (*it_t).assignedBlob.push_back(it_d->second);
                cout <<(*it_t).assignedBlob.size() << endl;
                (*it_t).invisibleCount = 0; 
                matchingFound = true;
            }
        }
        if (!matchingFound) unassigned.push_back(it_d->first);
    }
    updateTracks(matching);
    createNewTracks(det, unassigned);
    //mergeNearTracks();
}

void ObjTrack::updateTracks(map<int, avgCent> matches)
{
    // update tracks
    // --------------------------------------
    // For assigned tracks
    // - predict next centroid
    // - increase age and visibleCount
    // For lost tracks
    // - increase age and invisibleCount
    for (auto it = objs.begin(); it != objs.end(); /*++it*/)
    {
        (*it).age++;   // getting older

        if ((*it).assignedBlob.size() > 0)  // assigned tracks
        {
            // update Bbox
            //int minX = 999, minY = 999, maxX = 0, maxY = 0;
            //for (auto b = (*it).assignedBbox.begin();
                      //b != (*it).assignedBbox.end(); ++b)
            //{
                //if (minX > (*b).pt1.x) minX = (*b).pt1.x;
                //if (minY > (*b).pt1.y) minY = (*b).pt1.y;
                //if (maxX < (*b).pt2.x) maxX = (*b).pt2.x;
                //if (maxY < (*b).pt2.y) maxY = (*b).pt2.y;
                //cout <<(*b).pt1.x << ' ' << (*b).pt1.y << ' ' <<
                    //(*b).pt2.x << ' ' << (*b).pt2.y << endl;
            //}
            //(*it).bbox = bb{vec2i{minX, minY}, vec2i{maxX, maxY}};
            
            // compute center and predict 
            // for each assigned blob, calculate delta
            double meas_x = 0, meas_y = 0;
            for (auto it_b = (*it).assignedBlob.begin(); 
                      it_b != (*it).assignedBlob.end(); ++it_b)
            {
                meas_x += (*it_b).pt1.x;
                meas_y += (*it_b).pt1.y;
            }
            for (auto it_b = matches.begin(); it_b != matches.end(); ++it_b)
            {
                //vec2i t = (*it).centroids.back();
                //meas_x += t.x - ((it_b->second).accX / (it_b->second).n);
                //meas_y += t.y - ((it_b->second).accY / (it_b->second).n);
            }
            
            meas_x = meas_x / (*it).assignedBlob.size();
            meas_y = meas_y / (*it).assignedBlob.size();
            
            cout << "mx:" << meas_x << "my:" << meas_y << endl;;
            if ((*it).centroids.size() > 3) {
                auto it_c = (*it).centroids.rbegin();
                vec2d pred = (*it_c);
                vec2d pred2 = (*it_c++);
                float pred_x = meas_x + 0.9 * (pred.x - pred2.x) +  \
                                        0.1 * (meas_x - pred.x);
                float pred_y = meas_y + 0.9 * (pred.y - pred2.y) + \
                                        0.1 * (meas_y - pred.y);
                pred = vec2d{pred_x, pred_y};
                pred = vec2d{meas_x, meas_y};
                (*it).centroids.push_back(pred);

                int posPrev = (*it).pos;
                (*it).pos = isPosRight(vec2i{(int)pred.x, (int)pred.y});
                if (posPrev != 0 && posPrev != (*it).pos) {
                    cout << '#' << (*it).id << " crossed the line from ";
                    if (posPrev == 1 && (*it).pos == 2) 
                        cout << " left to right";
                    else if (posPrev == 2 && (*it).pos == 1)
                        cout << " right to left";
                    totalCross++;
                }

                cout << "p1x:" << pred.x << "p1y:" << pred.y;
                cout << "p2x:" << pred2.x << "p2y:" << pred2.y<< endl;
                cout << "px:" << pred_x << "py:" << pred_y<< endl;
                
            }
            else 
            {
                (*it).centroids.push_back(vec2d{meas_x, meas_y});
            }
            (*it).assignedBlob.clear();
            (*it).visibleCount++;
            it++;
        }
        else  // lost tracks
        {
            (*it).invisibleCount++;
            if ((*it).invisibleCount > 3)
                it = objs.erase(it);
            else 
                it++;
        }
    }
}

void ObjTrack::createNewTracks(map<int, bb> det, vector<int> list)
{
    // from the given list of unassigned detections, 
    // create new tracks
    for (auto it = list.begin(); it != list.end(); ++it)
    {
        bb b_ = det[(*it)];
        vec2d b_cent{(b_.pt2.x + b_.pt1.x) / 2., (b_.pt2.y + b_.pt1.y) / 2.};

        // selectively create new track by size
        if ((b_.pt2.x - b_.pt1.x) * (b_.pt2.y - b_.pt1.y) < TminSize) continue;

        movingObj2 new_;
        new_.id = lastTrackId++;
        new_.centroids.push_back(b_cent);
        new_.assignedBlob.push_back(b_);
        new_.age = 0;
        new_.visibleCount = 0;
        new_.invisibleCount = 0;
        new_.pos = 0;
        objs.push_back(new_);
    }
}
void ObjTrack::report(int t, int frmLen, IplImage* src)
{
    // report on console
    cout << "\n------------------------------------------------------------\n";
    cout << "Frame " << t << '/' << frmLen << " || ";
    cout << "# of objects crossed line: === " << totalCross << " ===\n" << endl;


    int cnt = 0;
    for (auto it = objs.begin(); it != objs.end(); ++it)
    {
        vec2d cent = ((*it).centroids.size() > 0) ? 
                                     (*it).centroids.back() : vec2d{-1, -1};
        cout << "[#" << (*it).id << "] at: (" << cent.x << ',' << cent.y 
            << ") age:" << (*it).age << " visible:" << (*it).visibleCount
            << " invisible:" << (*it).invisibleCount << " pos: ";

        if ((*it).pos == 0) cout << "Unknown" << endl;
        else if ((*it).pos == 1) cout << "Right" << endl;
        else cout << "Left" << endl;
        
        // draw bounding boxes on src image
        if ((*it).invisibleCount == 0) 
        {
            vec2d cent = (*it).centroids.back();
            bb b_{vec2i{(int)cent.x - 3, (int)cent.y -3}, 
                  vec2i{(int)cent.x + 3, (int)cent.y + 3}};
            draw_boundingBox( src, b_, 1 );
        }

    }
}

int ObjTrack::isPosRight(vec2i pt)
{
    //using cross product to determine which side the point is located at
    //compared to the line
    int det = ((loi_2.x - loi_1.x) * (pt.y - loi_1.y) - \
               (loi_2.y - loi_1.y) * (pt.x - loi_1.x));
    if (det < 0) return 2;
    else return 1;
}

void ObjTrack::draw_loi(IplImage* img, int color)
{
    uchar r, g, b;
    switch(color)
    {
        case 0:  r = 255; g = 255; b =   0; break;         // yellow
        case 1:  r = 255; g =   0; b =   0; break;         // red
        case 2:  r =   0; g = 255; b =   0; break;         // green 
        case 3:  r =   0; g =   0; b = 255; break;         // blue
        default: r = 255; g = 255; b = 255; break;         // white
    }

    float slope = abs(1.0 * (loi_2.y - loi_1.y) / (loi_2.x - loi_1.x));

    // assume in color channel
    int y = loi_1.y;
    float delta = 0.;
    for (int x = loi_1.x; x <= loi_2.x; ++x)
    {
        int idx = y * img->widthStep + 3 * x;
        img->imageData[idx] = b;
        img->imageData[idx + 1] = g;
        img->imageData[idx + 2] = r;

        delta += slope;
        if (delta >= 0.5)
        {
            y--;
            delta -= 1.0;
        }
    }
}

vector<vec2d> ObjTrack::estimateNumClusters(IplImage* img)
{
    // using standard k-means clustering distortion measure
    // sum_k ( sum_i ( distance^2 ) ),
    // where k is the number of clusters, for each datum, sum all the squared
    // distance. The assumption of k-means clustering is that it expects
    // same-sized clusters and adequate distance between them.

    // start from (kClusters-3) to max_kClusters to find optimum number of
    // clusters, assuming that no more than 3 objects can appear or disappear
    // on the next frame at the same time.
    float prevErr = -1.f;
    vector<vec2d> centroids_;
    for (int k = max(kClusters - 3, 1); k <= max_kClusters; ++k)
    {
        kClusters = k;
        vector<vec2d> centroids_k(centroids_);

        float err = runKmeansClustering(img, k, centroids_k);
        cout << endl << "K: " << k << ", err: " << err << endl;

        float derr = abs(err - prevErr);
        prevErr = err;
        if (err == 0) return centroids_;
        if ( derr < 10000 ) {
            cout << "before return size: " << centroids_.size() << endl;
            return centroids_;
        }
        centroids_ = centroids_k;
    }
    return centroids_;
}


int ObjTrack::runKmeansClustering(IplImage* img, int k, vector<vec2d>& centroids_)
{
    // run k means clustering by given number of clusters k,
    // return the distortion measure, and store centers of the
    // clustering in centroids

    // initialize centroids
    char* data = img->imageData;
    int w_ = img->width;
    int h_ = img->height;   // bg_sub is in BGR mode
    int ch = img->nChannels;

    // initialize first k centroids
    while (centroids_.size() != k) 
    {
        if (centroids_.size() < k)
        {
            centroids_.push_back(vec2d{(double)(rand() % w_), 
                                       (double)(rand() % h_)});
        }
        else
        {
            centroids_.pop_back();
        }
    }

    int err;
    vector<int> numPerCluster( k );
    vector<uchar> assignment( w_ * h_, max_kClusters );
    cout << "100 assn:" << (int)assignment[100] << endl;
    bool pxMoved = true;
    // using "k-means algorithm" instead of "h-means"
    // (http://people.sc.fsu.edu/~jburkardt/f_src/kmeans/kmeans.html)
    //
    // run until no pixel moved to another cluster
    int cnt = 0;
    while (pxMoved)
    {
        err = 0;
        cnt = 0;
        pxMoved = false;
        // for each pixel
        for (int y = 0; y < h_; ++y)
        {
            for (int x = 0; x < w_; ++x)
            {
                int idx = y * w_ + x;
                if (data[ch * idx] != (char)255) continue;
                // compare
                float minDist = pow(h_, 2) + pow(w_, 2);
                int minCent;
                // to each centroid
                for (auto it = centroids_.begin(); it != centroids_.end(); ++it)
                {
                    float dist = pow(x - (*it).x, 2) + pow(y - (*it).y, 2);
                    if (dist < minDist)
                    {
                        minDist = dist;
                        minCent = it - centroids_.begin();
                    }
                }
                int prevC = assignment[idx];
                if (minCent == prevC)
                {
                    err += minDist;
                    cnt++;
                    continue;  // this might be the problem
                }

                // swap
                pxMoved = true;
                // update previous cluster
                if (prevC == max_kClusters) 
                { // if pixel not assigned yet, do nothing on previous cluster
                }
                else
                {
                    if (numPerCluster[prevC] > 1)
                    {
                        cout << '3';
                        centroids_.at(prevC).x = 
                            (centroids_.at(prevC).x * numPerCluster[prevC] - x) \
                            / (numPerCluster[prevC] - 1);
                        centroids_.at(prevC).y = 
                            (centroids_.at(prevC).y * numPerCluster[prevC] - y) \
                            / (numPerCluster[prevC] - 1);
                        numPerCluster[prevC]--;
                    }
                }

                // update new cluster
                int newC = minCent;
                centroids_.at(newC).x = 
                    (centroids_.at(newC).x * numPerCluster[newC] + x) \
                    / (numPerCluster[newC] + 1);
                centroids_.at(newC).y = 
                    (centroids_.at(newC).y * numPerCluster[newC] + y) \
                    / (numPerCluster[newC] + 1);
                assignment[idx] = newC;
                numPerCluster[newC]++;
            }
        } // for each pixel
    } // while pxMoved
    cout << "count: " << cnt << endl;
    cout << "centroids size: " << centroids_.size() << endl;
    for (auto it = centroids_.begin(); it != centroids_.end(); ++it)
    {
        cout << (*it).x << ' ' << (*it).y << endl;
    }
    return err;
}
