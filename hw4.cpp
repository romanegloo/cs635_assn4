#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <stdlib.h>   // rand
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "common.h"
#include "bg_GMM.h"
#include "obj_tracking.h"
#include "anl_crossline.h"

const int useCV = true;

using namespace cv;
using namespace std;

class ObjDetector
{
    private:
        int totalCross{0};
        vector<movingObj> objs_track;
    public:
        vec2i loi_1, loi_2;         // line of interest

    public:
        ObjDetector();
        void update(map<int, bb>);
        void drawBB(IplImage*);
        void report(int t);
        bool checkPos(vec2i);
};

// global variables
char* ptnInputImg;
int tStart, frmLen;
int imgW, imgH;
vector<IplImage*> srcData;

// parameters
const int BG_RANGE = 60;   // background subtraction range in time frame
const int DS_RATIO = 1;    // simple n x n averaging downsmaple. 
                           // If the ratio is 1, no downsampling done.
const float DIFF_THS = 0.1;  // threshold for image difference
const float DIST_OBJ = 30;    // distance between objects
const float SIZE_DIFF_ALLW = 0.05;   // size difference of objects allowed

// function prototypes
int load_srcImages( int verb );
bool fileExist( string fileName );
IplImage* bg_median( int t, vector<IplImage*> src );
IplImage* downsample( IplImage* );
IplImage* bg_subtract( IplImage* src, IplImage* bg );
void apply_opening_closing( IplImage* img );
void apply_closing_opening( IplImage* img );
void apply_closing( IplImage* img );
void apply_opening( IplImage* img );
void apply_dilation( IplImage* img );
void apply_erosion( IplImage* img );
vector<bb> drawBB_obj( IplImage* img, int t );
map<int, bb> connected_comp_labeling( IplImage* bg_sub, int t );

int CVmin( IplImage* img1, IplImage* img2, IplImage* dest );
int CVmax( IplImage* img1, IplImage* img2, IplImage* dest );
unsigned char bgr2gray( IplImage* img, int x, int y );
void draw_boundingBox( IplImage* img, bb obj, int color );
bool isLeft( vec2i l1, vec2i l2, vec2i pt );
template<class InputIt1, class InputIt2>
bool intersect(InputIt1, InputIt1, InputIt2, InputIt2);
float boxSize(vec2i, vec2i);
void draw_line(vec2i pt1, vec2i pt2, IplImage* img);

template<typename T>
float l2_distance(T pt1, T pt2)
{
    return sqrt(pow((pt1.x - pt2.x), 2) + pow((pt1.y - pt2.y), 2));
}

// command line syntax
// Usage:
// ------
// hw4.exe LOI_x0 LOI_y0 LOI_x1 LOI_y1 image_name_template \
//             start_number total_image_number
//             
// parameters:
// -----------
// (LOI_x0 ~ LOI_y1):   two points of the line of interest from 
//                      (x0, y0) to (x1, y1)
// image_name_template: filename pattern to be used for input images
// start_number:        time frame to start from
// total_image_number:  the length of frames

int main( int argc, char** argv )
{
    srand (time(NULL));

    vec2i loi1{stoi(argv[1]) / DS_RATIO, stoi(argv[2]) / DS_RATIO};
    vec2i loi2{stoi(argv[3]) / DS_RATIO, stoi(argv[4]) / DS_RATIO};
    ptnInputImg = argv[5];
    tStart = stoi(argv[6]);
    frmLen = stoi(argv[7]);

    ObjTrack objT(loi1, loi2);

    if (load_srcImages(1)) {
        cerr << "[Error] Unable to load source images" << endl;
        return -1;
    }

    BGD_GMM bgd(imgW, imgH);
    bgd.fAlpha = 0.01f;

    // get rectangular area surrounding crossline
    Anl_CrossLine ACL(loi1, loi2, imgW, imgH);
    //ACL.drawAOI(srcData.at(t - tStart));


    IplImage* bg = cvCreateImage(cvSize(imgW, imgH), IPL_DEPTH_8U, 3);
    IplImage* bg_sub = cvCreateImage(cvSize(imgW, imgH), IPL_DEPTH_8U, 3);

    /*
     * image segmentation
     * ------------------
     * a) image difference
     * b) threshold of the difference image
     * c) image morphology
     * d) connected component analysis
     */

    if (useCV)
    {
        namedWindow("bg sub", WINDOW_AUTOSIZE);
        moveWindow("bg sub", 0, 550);
        namedWindow("bg", WINDOW_AUTOSIZE);
        moveWindow("bg", 0, 300);
        namedWindow("src", WINDOW_AUTOSIZE);
        moveWindow("src", 0, 50);
    }

    // extract the background using GMM for the entire frame set first
    // and then use the same gaussian distribution set to extract foregournd
    cout << "*\n* computing background image\n";
    for (int t = 0; t < frmLen; ++t)
    {
        bgd.bgdUpdatePixelGD(srcData.at(t)->imageData,
                             bg->imageData,
                             bg_sub->imageData);
        cout << '.' << flush;
    }

    // generate heatmap for lanes
    cout << "*\n* Computing Heatmap to Construct Lanes \n";
    for (int t = tStart; t < tStart + frmLen; ++t)
    {
        // background substraction
        bgd.bgdUpdatePixelGD(srcData.at(t - tStart)->imageData,
                             bg->imageData,
                             bg_sub->imageData);

        // apply opening of the closing
        apply_closing_opening(bg_sub);
        ACL.updateHeatmap(bg_sub->imageData);
        cout << '.' << flush;
    }
    cout << endl;
    ACL.computeLanes();

    // for each frame
    for (int t = tStart; t < tStart + frmLen; ++t)
    {
        // background substraction
        // IplImage* bg_sub = bg_subtract(srcData.at(t), bg);
        bgd.bgdUpdatePixelGD(srcData.at(t - tStart)->imageData,
                             bg->imageData,
                             bg_sub->imageData);

        // apply opening of the closing
        apply_closing_opening(bg_sub);
        
        // two-pass 8-connectivity
        map<int, bb> blobs = connected_comp_labeling(bg_sub, t);
        for (auto it = blobs.begin(); it != blobs.end(); ++it) 
            draw_boundingBox(bg_sub, it->second, 3);

        // update detector, which controls all the moving objects
        //objT.assignDetections(blobs);
        //objT.report(t, frmLen, srcData.at(t - tStart));
        //objT.draw_loi(srcData.at(t - tStart), objDet.loi_1, objDet.loi_2, 3);

        // Totally different way (k-means clustering)
        //cout << "Frame " << t << '/' << frmLen << " || ";
        //vector<vec2d> centroids = objT.estimateNumClusters(bg_sub);
        //for (auto it = centroids.begin(); it != centroids.end(); ++it) 
        //{
            //bb b_{vec2i{(int)(*it).x - 3, (int)(*it).y - 3},  \
                  //vec2i{(int)(*it).x + 3, (int)(*it).y + 3}};
            //draw_boundingBox( srcData.at(t - tStart), b_, 1 );
        //}
        objT.draw_loi(srcData.at(t - tStart), 1);

        ACL.analyze(bg_sub->imageData, srcData.at(t - tStart)->imageData, 
                    t - tStart);

        if (useCV)
        {
            cvShowImage("bg sub", bg_sub);
            cvShowImage("bg", bg);
            cvShowImage("src", srcData.at(t - tStart));
            cvWaitKey(0);
        }
        else {
            cin.get();
        }

    }

    if (useCV) cvDestroyAllWindows();
}

int load_srcImages(int verb) {
    // check if all the input images exist
    if (verb) cout << "*\n* Checking Input Files \n";

    for (int i = 0; i < frmLen; ++i)
    {
        // std::string formatting like sprintf 
        // [http://stackoverflow.com/a/26221725/335102]
        size_t size = snprintf( nullptr, 0, ptnInputImg, i + tStart) + 1;
        unique_ptr<char[]> buf( new char[ size ] ); 
        snprintf( buf.get(), size, ptnInputImg, i + tStart );
        string fileName = string( buf.get(), buf.get() + size - 1 ); 
        if (fileExist(fileName)) 
        {
            if (verb) cout << '.' << flush;
            //srcData.push_back(imread(fileName, 1));
            IplImage* img = cvLoadImage(fileName.c_str(), 1);
            srcData.push_back(downsample(img));
        } else {
            if (verb) cout << 'x' << flush;
            return -1;
        }
    }
    cout << endl;

    // some meta data
    imgW = srcData.at(0)->width;
    imgH = srcData.at(0)->height;

    return 0;
}

bool fileExist(string fileName)
{
    std::ifstream infile(fileName.c_str());
    return infile.good();
}

IplImage* downsample(IplImage* img)
{
    if (DS_RATIO > 1) {
        // compute destination size, rename them for convenience
        int w_ = img->width;
        int h_ = img->height;
        int ws_ = img->widthStep;
        int ch_ = img->nChannels;
        int ds = DS_RATIO; 
        int w = w_ / ds;
        int h = h_ / ds;

        IplImage *newImg = cvCreateImage(cvSize(w, h), IPL_DEPTH_8U, 3);
        for (int y = 0; y < h; ++y) {  // y-coord
            for (int x = 0; x < w; ++x) {  // x-coord
                for (int c = 0; c < ch_; ++c) {  // channel
                    int sumV = 0;
                    for (int dy = 0; dy < ds; ++dy) { // downsample-y
                        for (int dx = 0; dx < ds; ++dx) {  // downsample-x
                            int idx = ds * (ws_ * y  + ch_ * x) + \
                                      (ws_ * dy + dx * ch_) + c;
                            sumV += (uchar) img->imageData[idx];
                        }
                    }
                    newImg->imageData[ch_ * (y * w + x) + c] = \
                                            char (sumV / DS_RATIO / DS_RATIO);
                }
            }
        }
        return newImg;
    }
    return img;
}

IplImage* bg_median(int t, vector<IplImage*> src)
{
    // parameter t (time frame) is not used here, but may be used later version

    cout << "*\n* Computing Background - median (may take a while)\n*\n";

    IplImage* tmp = cvCreateImage(cvSize(imgW, imgH), IPL_DEPTH_8U, 3);

    int itFrom = (t < BG_RANGE) ? 0 : t - BG_RANGE;
    cout << "Analyzing frames: ";
    for (int i = itFrom; i < itFrom + BG_RANGE; ++i)
    {
        cout << '.' << flush;
        for (int j = i + 1; j < itFrom + BG_RANGE; ++j)
        {
            memcpy(tmp->imageData, src.at(i)->imageData, \
                       src.at(i)->imageSize);
            // test CVmin, CVmax
            CVmin(src.at(i), src.at(j), src.at(i));
            CVmax(src.at(j), tmp, src.at(j));
        }
    }
    cout << endl;
    IplImage* rtn = src.at(BG_RANGE / 2);

    return rtn; 
}

IplImage* bg_subtract(IplImage* src, IplImage* bg)
{
    // assume in bgr color for now
    IplImage* bg_sub = cvCreateImage(cvSize(imgW, imgH), IPL_DEPTH_8U, 3);

    // for each pixel
    for (int y = 0; y < imgH; ++y) 
    {
        for (int x = 0; x < imgW; ++x)
        {
            int idx = 3 * (y * imgW + x);
            uchar bd = abs(src->imageData[idx] - bg->imageData[idx]);
            uchar gd = abs(src->imageData[idx + 1] - bg->imageData[idx + 1]);
            uchar rd = abs(src->imageData[idx + 2] - bg->imageData[idx + 2]);
            //float diff = (bd + gd + rd) / 3. / 255.;

            // intensity difference
            float diff = abs(bgr2gray(src, x, y) - bgr2gray(bg, x, y)) / 255.;

            bg_sub->imageData[idx] = (diff > DIFF_THS) ? 255 : 0;
            bg_sub->imageData[idx + 1] = (diff > DIFF_THS) ? 255 : 0;
            bg_sub->imageData[idx + 2] = (diff > DIFF_THS) ? 255 : 0;
        }
    }

    return bg_sub;
}

/*
 * Image Morphology
 */
void apply_opening_closing(IplImage* img)
{
    apply_opening(img);
    apply_closing(img);
}

void apply_closing_opening(IplImage* img)
{
    apply_closing(img);
    apply_opening(img);
}

void apply_closing(IplImage* img)
{
    apply_erosion(img);
    apply_erosion(img);
    apply_dilation(img);
    apply_dilation(img);
}

void apply_opening(IplImage* img)
{
    apply_dilation(img);
    apply_dilation(img);
    apply_erosion(img);
    apply_erosion(img);
}

void apply_dilation(IplImage* img)
{
    int ch = img->nChannels;
    int ws = img->widthStep;

    // structuring element is 3x3 matrix with ones.
    // Here, I will just check 3x3 relative coordinates, without defining the
    // structuring element.

    // clone
    uchar* imgClone = new uchar[img->imageSize];
    memcpy(imgClone, img->imageData, img->imageSize);

    for (int y = 0; y < imgH; ++y)
    {
        for (int x = 0; x < imgW; ++x)
        {
            int idx = ch * (y * imgW + x);
            // check 9 elements
            // If any of them is a foreground, mark it as foreground 
            // If all of them are background, leave the pixel as it is.
            for (int ry = -1; ry <=1; ++ry)
            {
                for (int rx = -1; rx <= 1; ++rx)
                {
                    int ridx = idx + ry * ws + rx * ch;
                    // boundary case
                    if (x + rx < 0 || x + rx >= imgW || \
                        y + ry < 0 || y + ry >= imgH) continue;
                    if (imgClone[ridx] == 255)
                    {
                        for (int c = 0; c < ch; ++c)
                        {
                            img->imageData[idx + c] = 255;
                        }
                        rx = 2; ry = 2;  // exit loop
                    }
                }
            }
        }
    }
}

void apply_erosion(IplImage* img)
{
    int ch = img->nChannels;
    int ws = img->widthStep;

    // structuring element is 3x3 matrix with ones.
    // Here, I will just check 3x3 relative coordinates, without defining the
    // structuring element.

    // clone
    uchar* imgClone = new uchar[img->imageSize];
    memcpy(imgClone, img->imageData, img->imageSize);

    for (int y = 0; y < imgH; ++y)
    {
        for (int x = 0; x < imgW; ++x)
        {
            int idx = ch * (y * imgW + x);
            // check 9 elements
            // If any of them is not a foreground, mark it as background
            // If all of them is foregrounds, leave the pixel as it is.
            for (int ry = -1; ry <=1; ++ry)
            {
                for (int rx = -1; rx <= 1; ++rx)
                {
                    int ridx = idx + ry * ws + rx * ch;
                    // boundary case
                    if (x + rx < 0 || x + rx >= imgW || \
                        y + ry < 0 || y + ry >= imgH || \
                        imgClone[ridx] < 255)
                    {
                        for (int c = 0; c < ch; ++c)
                        {
                            img->imageData[idx + c] = 0;
                        }
                        rx = 2; ry = 2;  // exit loop
                    }
                }
            }
        }
    }
}

map<int, bb>  connected_comp_labeling( IplImage* bg_sub, int t )
{
    map<int, bb> Bboxes;
    map<int, set<int>> linked;
    int labels[imgW * imgH];
    int ch = bg_sub->nChannels;
    int nextLabel = 0;
    char* data = bg_sub->imageData;
    char wh = (char) 255;

    // first pass
    for (int y = 0; y < imgH; ++y)
    {
        for (int x = 0; x < imgW; ++x)
        {
            // if foreground
            int idx = y * imgW + x;
            set<int> n_8;
            if (data[ch * idx] == wh)
            {
                /*
                // 4-connectivity
                // no neighbor
                int up = labels[idx - imgW];
                int left = labels[idx - 1];
                if (up == -1 && left == -1)
                {
                    cout << "nextLabel: " << nextLabel << ' ' << x << ' ' << y;
                    linked.insert({nextLabel, set<int>{nextLabel}});
                    labels[idx] = nextLabel++;  // assign new label
                }
                else if (up == left)
                    labels[idx] = up;
                else
                {
                    if (up == -1) labels[idx] = left;
                    else if (left == -1) labels[idx] = up;
                    else 
                    {
                        if (up < left) labels[idx] = up;
                        else labels[idx] = left;
                        linked[up].insert(left);
                        linked[left].insert(up);
                    }
                }
                */

                // find neighbors (8-connectivity)
                // N-E
                if (data[ch * (idx - imgW + 1)] == wh)
                    n_8.insert(labels[idx - imgW + 1]);
                // N
                if (data[ch * (idx - imgW)] == wh)
                    n_8.insert(labels[idx - imgW]);
                // N-W
                if (data[ch * (idx - imgW - 1)] == wh)
                    n_8.insert(labels[idx - imgW - 1]);
                // W
                if (data[ch * (idx - 1)] == wh)
                    n_8.insert(labels[idx - 1]);

                //// if neighbor is empty
                if (n_8.empty()) 
                {
                    linked.insert({nextLabel, set<int>{nextLabel}});
                    labels[idx] = nextLabel++;  // assign new label
                }
                else
                {
                    // find the smallest label
                    labels[idx] = *(n_8.begin());
                    // for each neighbors' label, set union of linked labels
                    for (auto it = n_8.begin(); it != n_8.end(); ++it)
                    {
                        set<int> ll;
                        set_union(linked[*it].begin(), linked[*it].end(),
                                  n_8.begin(), n_8.end(), inserter(ll, ll.begin()));
                        linked[*it] = ll;
                    }
                }
            }
            else labels[idx] = -1;
        }
    }

    // create unique label classes
    map<int, set<int>> labelClass;
    int classID = 0;
    for (auto it = linked.begin(); it != linked.end(); ++it)
    {
        set<int> s1 = it->second;
        bool isConnected = false;
        for (auto it2 = labelClass.begin(); it2 != labelClass.end(); ++it2)
        {
            set<int> s2 = it2->second;
            // connection found
            if (intersect(s1.begin(), s1.end(), s2.begin(), s2.end()))
            {
                (it2->second).insert(s1.begin(), s1.end());
                isConnected = true;
            }
        }
        // if no connected label, create new label
        if (!isConnected) labelClass.insert({classID++, it->second});
    }
        

    // second pass
    for (int y = 0; y < imgH; ++y)
    {    
        for (int x = 0; x < imgW; ++x)
        {
            // if foreground, relabel with the lowest equivalent label
            int idx = y * imgW + x;
            if (data[ch * idx] == wh)
            {
                for (auto it = labelClass.begin(); it != labelClass.end(); ++it)
                {
                    if ((it->second).find(labels[idx]) != (it->second).end())
                    {
                        labels[idx] = it->first;
                        break;
                    }
                }
                // create Bbox
                auto it = Bboxes.find(labels[idx]);
                if ( it == Bboxes.end() )
                {
                    // create new Bbox
                    Bboxes.insert({labels[idx], bb{vec2i{x, y}, vec2i{x, y}}});
                }
                else 
                {
                    if (x < (it->second).pt1.x) (it->second).pt1.x = x;
                    if (x > (it->second).pt2.x) (it->second).pt2.x = x;
                    if (y < (it->second).pt1.y) (it->second).pt1.y = y;
                    if (y > (it->second).pt2.y) (it->second).pt2.y = y;
                }
            }
        }

    }

    // if blob is too small, consider as noise
    for (auto it = Bboxes.begin(); it != Bboxes.end(); /*++it*/) 
    {
        auto b = it->second;
        if (boxSize(b.pt1, b.pt2) < 64 / DS_RATIO / DS_RATIO )
            it = Bboxes.erase(it);
        else ++it;
    }
    return Bboxes;
}

vector<bb> drawBB_obj(IplImage* bg_sub, int t)
{
    unordered_map<int, bool> fg_covered;     // key: index, bool: covered 
    vector<bb> blobs;
    int ch = bg_sub->nChannels;
    int ws = bg_sub->widthStep;
    int bbId = 0;

    // initialize fg_covered with all the foreground pixels
    for (int y = 0; y < imgH; ++y)
    {
        for (int x = 0; x < imgW; ++x)
        {
            int idx = y * ws + ch * x;
            if ((uchar)bg_sub->imageData[idx] == 255 )
            {
                fg_covered.insert({idx, 0});
                if (idx < 0) cout << idx << endl;
            }
        }
    }

    // iterate fg_covered
    for (auto poi = fg_covered.begin(); poi != fg_covered.end(); ++poi)
    {
        if (poi->second) continue;    // if the pixel is already covered 

        // else find near-by foregrounds
        unordered_set<int> local_blob{poi->first};
        // initialize bb region
        int minX = imgW, maxX = -1, minY = imgH, maxY = -1;

        while (!local_blob.empty())  // until there's no near-by fg pixels
        {
            // pop one pixel to examine
            auto local_poi = local_blob.begin();
            int poi_x = (*local_poi % ws) / ch;
            int poi_y = *local_poi / ws;

            // allow 3 pixel gap between fg blobs, so check all the
            // surrounding 9x9 neighbors
            for (int y = -4; y <= 4; ++y)
            {
                for (int x = -4; x <= 4; ++x)
                {
                    // clamp
                    if (poi_x + x < 0 || poi_x + x >= imgW) continue;
                    if (poi_y + y < 0 || poi_y + y >= imgH) continue;
                    
                    // calculate relative index
                    int ridx = *local_poi + y * ws + x * ch;
                    // only care foreground pixels
                    if ((uchar)bg_sub->imageData[ridx] == 0) continue;
                    // check if the pixel is already covered
                    if (fg_covered[ridx]) continue;
                    else {
                        // add to current blob
                        local_blob.insert(ridx);
                        fg_covered[ridx] = true;
                        // update bb dimension
                        if (minX > poi_x + x) minX = poi_x + x;
                        if (maxX < poi_x + x) maxX = poi_x + x;
                        if (minY > poi_y + y) minY = poi_y + y;
                        if (maxY < poi_y + y) maxY = poi_y + y;
                    }
                }
            }
            // erase current pixel
            local_blob.erase(local_poi);
        }
        // big enough to be a blob?
        if ((maxX - minX) * (maxY - minY) >= 25) {
            // create blob
            bb obj{vec2i{minX, minY}, vec2i{maxX, maxY}};
            blobs.push_back(obj);
            // draw bounding box
            draw_boundingBox(bg_sub, obj, 0);
        }
    }

    return blobs;
}



void draw_boundingBox(IplImage* img, bb obj, int color)
{
    uchar r, g, b;
    switch(color)
    {
        case 0:  r = 255; g = 255; b =   0; break;         // yellow
        case 1:  r = 255; g =   0; b =   0; break;         // red
        case 2:  r =   0; g = 255; b =   0; break;         // green 
        case 3:  r =   0; g =   0; b = 255; break;         // blue
        default: r = 255; g = 255; b = 255; break;         // white
    }

    // assume in color channel
    for (int x = obj.pt1.x; x <= obj.pt2.x; ++x)
    {
        for (int y = obj.pt1.y; y <= obj.pt2.y; ++y)
        {
            if (x < 0 || x >= imgW || y < 0 || y >= imgH ) continue;
            if (x == obj.pt1.x || x == obj.pt2.x || \
                y == obj.pt1.y || y == obj.pt2.y )
            {

                int idx = y * img->widthStep + 3 * x;
                img->imageData[idx] = b;
                img->imageData[idx + 1] = g;
                img->imageData[idx + 2] = r;
            }
        }
    }
}

/*
 * Helper Functions
 */
int CVmin(IplImage* img1, IplImage* img2, IplImage* dest)
{
    int w = img1->width;
    int h = img1->height;
    int ch = img1->nChannels;
    int ws = img1->widthStep;

    if (ch == 1) {
        for (int y = 0; y < h; ++y)
        {
            for (int x = 0; x < ws; ++x)
            {
                int idx = ws * y + x;
                dest->imageData[idx] = \
                    (img1->imageData[idx] > img2->imageData[idx]) ? \
                        img2->imageData[idx] : img1->imageData[idx];
            }
        }
    } else {
        for (int y = 0; y < h; ++y)
        {
            for (int x = 0; x < w; ++x)
            {
                int idx = ch * (w * y + x);
                if (bgr2gray(img1, x, y) > bgr2gray(img2, x, y))
                {
                    for (int c = 0; c < ch; ++c)
                    {
                        dest->imageData[idx + c] = img2->imageData[idx + c];
                    }
                } else {
                    for (int c = 0; c < ch; ++c)
                    {
                        dest->imageData[idx + c] = img1->imageData[idx + c];
                    }
                }
            }
        }
    }
    return 0;
}

int CVmax(IplImage* img1, IplImage* img2, IplImage* dest)
{
    int w = img1->width;
    int h = img1->height;
    int ch = img1->nChannels;
    int ws = img1->widthStep;

    if (ch == 1) {
        for (int y = 0; y < h; ++y)
        {
            for (int x = 0; x < ws; ++x)
            {
                int idx = ws * y + x;
                dest->imageData[idx] = \
                    (img1->imageData[idx] > img2->imageData[idx]) ? \
                        img1->imageData[idx] : img2->imageData[idx];
            }
        }
    } else {
        for (int y = 0; y < h; ++y)
        {
            for (int x = 0; x < w; ++x)
            {
                int idx = ch * (w * y + x);
                if (bgr2gray(img1, x, y) > bgr2gray(img2, x, y))
                {
                    for (int c = 0; c < ch; ++c)
                    {
                        dest->imageData[idx + c] = img1->imageData[idx + c];
                    }
                } else {
                    for (int c = 0; c < ch; ++c)
                    {
                        dest->imageData[idx + c] = img2->imageData[idx + c];
                    }
                }
            }
        }
    }
    return 0;
}

unsigned char bgr2gray(IplImage* img, int x, int y)
{
    int ch = img->nChannels;
    int w = img->width;

    if (ch != 3) {
        cerr << "[Error] not in rgb color mode" << endl;
        return 0;
    }

    unsigned char b = img->imageData[y * ch * w + x * ch];
    unsigned char g = img->imageData[y * ch * w + x * ch + 1];
    unsigned char r = img->imageData[y * ch * w + x * ch + 2];

    float rtn = 0.299 * r + 0.587 * g + 0.114 * b;
    return (unsigned char) rtn;
}

float boxSize(vec2i p1, vec2i p2)
{
    return abs(p1.x - p2.x) * abs(p1.y - p2.y);
}

/*
 * Define ObjDetector Class
 */
ObjDetector::ObjDetector(){};
void ObjDetector::update(map<int, bb> bbs_t) 
{
    // visited bounding boxes
    unordered_set<int> matched_bb;
    unordered_set<int> unique_obj;

    // loop over movingObj vector
    for (auto it_m = objs_track.begin(); it_m != objs_track.end();)
    {
        // reset movingObj dimension
        (*it_m).pt1.x = imgW; (*it_m).pt2.x = -1;
        (*it_m).pt1.y = imgH; (*it_m).pt2.y = -1;

        bool matchExist = false;
        // read bounding boxes
        for (auto it_b = bbs_t.begin(); it_b != bbs_t.end(); ++it_b)
        {
            bb b_ = it_b->second;

            // center of the box
            vec2i b_cent{(b_.pt2.x + b_.pt1.x) / 2, \
                         (b_.pt2.y + b_.pt1.y) / 2};
            float dist = l2_distance(b_cent, (*it_m).cent);
            if (dist > DIST_OBJ) continue;

            // if they are close enough, update movingObj
            int idx = b_.pt1.y * imgW + b_.pt1.x;
            matched_bb.insert(idx);
            matchExist = true;
            if (b_.pt1.x < (*it_m).pt1.x) (*it_m).pt1.x = b_.pt1.x;
            if (b_.pt1.y < (*it_m).pt1.y) (*it_m).pt1.y = b_.pt1.y;
            if (b_.pt2.x > (*it_m).pt2.x) (*it_m).pt2.x = b_.pt2.x;
            if (b_.pt2.y > (*it_m).pt2.y) (*it_m).pt2.y = b_.pt2.y;

            // re compute center
            (*it_m).cent.x = ((*it_m).pt1.x + (*it_m).pt2.x) / 2;
            (*it_m).cent.y = ((*it_m).pt1.y + (*it_m).pt2.y) / 2;
            (*it_m).status = "active";
            bool newPos = checkPos((*it_m).cent);
            if (newPos != (*it_m).pos_isLeft) {
                totalCross++;
            }
            (*it_m).pos_isLeft = newPos;
        }
        // if there's no matching bounding box, mark it 'disappeared'
        if (!matchExist) {
            if ((*it_m).status == "disappeared") objs_track.erase(it_m);
            else (*it_m).status = "disappeared";
        }

        // remove duplicates (merged ones)
        int idx = (*it_m).pt1.y * imgW + (*it_m).pt1.x;
        auto found = unique_obj.find(idx);
        if (found == unique_obj.end()) {
            unique_obj.insert(idx);
            ++it_m;
        } 
        else 
        {
            it_m = objs_track.erase(it_m);
        }
    }

    // loop over bounding boxes, and check if new object exists
    for (auto it_b = bbs_t.begin(); it_b != bbs_t.end(); ++it_b)
    {
        bb b_ = it_b->second;
        int idx = b_.pt1.y * imgW + b_.pt1.x;
        auto found = matched_bb.find(idx);
        if (found == matched_bb.end())
        {
            // new bounding box
            movingObj obj;
            vec2i b_cent{(b_.pt2.x + b_.pt1.x) / 2, \
                         (b_.pt2.y + b_.pt1.y) / 2};
            obj.cent = b_cent;
            obj.pt1 = b_.pt1;
            obj.pt2 = b_.pt2;
            obj.status = "active";
            obj.pos_isLeft = checkPos(b_cent);
            objs_track.push_back(obj);
        }
    }
}

void ObjDetector::report(int t) 
{
    cout << "\n------------------------------------------------------------\n";
    cout << "Frame " << t << '/' << frmLen << " || ";
    cout << "# of objects crossed line: === " << totalCross << " ===\n" << endl;


    int cnt = 0;
    for (auto it = objs_track.begin(); it != objs_track.end(); ++it)
    {
        cout << "- obj <" << (*it).cent.x << ", " << (*it).cent.y 
            << ">, Status: " << (*it).status << ", Side: ";
        if ((*it).pos_isLeft) cout << "Left" << endl;
        else cout << "Right" << endl;
        cnt++;
    }
    cout << "Total objects on screen: " << cnt << endl;

}

bool ObjDetector::checkPos(vec2i pt) 
{
    // using cross product to determin which side the point is located
    // compared to the line
    int det = ((loi_2.x - loi_1.x) * (pt.y - loi_1.y) - \
               (loi_2.y - loi_1.y) * (pt.x - loi_1.x));
    return (det < 0);
}

void ObjDetector::drawBB(IplImage* img)
{
    for (auto it_m = objs_track.begin(); it_m != objs_track.end(); ++it_m)
    {
        bb obj{(*it_m).pt1, (*it_m).pt2};
        if ((*it_m).status == "active") draw_boundingBox(img, obj, 1);
    }
}

template<class InputIt1, class InputIt2>
bool intersect(InputIt1 first1, InputIt1 last1, InputIt2 first2, InputIt2 last2)
{
    while (first1 != last1 && first2 != last2) {
        if (*first1 < *first2) {
            ++first1;
            continue;
        } 
        if (*first2 < *first1) {
            ++first2;
            continue;
        } 
        return true;
    }
    return false;
}

