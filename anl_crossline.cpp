#include <iostream>
#include <vector>
#include <algorithm>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "common.h"
#include "anl_crossline.h"

using namespace std;

Anl_CrossLine::Anl_CrossLine(vec2i pt1, vec2i pt2, int w_, int h_)
                          :loi_1{pt1}, loi_2{pt2}, imgW{w_}, imgH{h_}
{
    aoi = new uchar[imgW * imgH]{0};
    gap = 40;
    getAOI();
    for (int x = loi_1.x; x < loi_2.x; ++x)
        heatmap.push_back(0);
    totalCross = 0;
    if (loi_1.x == 237 && loi_1.y == 372) slopeInv = -30;
    else if (loi_1.x == 167 && loi_1.y == 387) slopeInv = -50;
    else slopeInv = 0;
}

int Anl_CrossLine::getAOI()
{
    // compute the four corners of the area
    float slope = 1.f * (loi_2.y - loi_1.y) / (loi_2.x - loi_1.x);
    float den = sqrt(pow(slope, 2) + 1);
    int p1_x = loi_1.x - (gap / den);
    int p2_x = loi_1.x + (gap / den);
    int p3_x = loi_2.x - (gap / den);
    int p4_x = loi_2.x + (gap / den);
    int p1_y = loi_1.y + (gap * slope / den);
    int p2_y = loi_1.y - (gap * slope / den);
    int p3_y = loi_2.y + (gap * slope / den);
    int p4_y = loi_2.y - (gap * slope / den);

    aoi_p1 = vec2i{p1_x, p1_y};
    aoi_p2 = vec2i{p2_x, p2_y};
    aoi_p3 = vec2i{p3_x, p3_y};
    aoi_p4 = vec2i{p4_x, p4_y};

    // create mask
    for (int y = 0; y < imgH; ++y)
    {
        for (int x = 0; x < imgW; ++x)
        {
            vec2i pt{x, y};
            if ( isPosRight(aoi_p2, aoi_p1, pt) && \
                 !isPosRight(aoi_p2, aoi_p4, pt) && \
                 !isPosRight(aoi_p4, aoi_p3, pt) && \
                 isPosRight(aoi_p1, aoi_p3, pt) )
            {
                aoi[y * imgW + x] = 255;
            }
        }
    }
    return 0;
}

int Anl_CrossLine::drawAOI(IplImage* img)
{
    int ch = img->nChannels;

    for (int y = 0; y < imgH; ++y)
    {
        for (int x = 0; x < imgW; ++x)
        {
            if (aoi[y * imgW + x] > 0)
                img->imageData[ch * (y * imgW + x) + 2] = 255;
        }
    }
    return 0;
}

bool Anl_CrossLine::isPosRight(vec2i l1, vec2i l2, vec2i pt)
{
    //using cross product to determine which side the point is located at
    //compared to the line
    int det = ((l2.x - l1.x) * (pt.y - l1.y) - (l2.y - l1.y) * (pt.x - l1.x));
    return (det > 0);
}

int Anl_CrossLine::updateHeatmap(char* data)
{
    float a = (float) (loi_2.x - loi_1.x);
    float b = (float) (loi_2.y - loi_1.y);
    float slope = abs(b / a);
    int y = loi_1.y;
    float delta = 0.f;
    int cnt = 0;
    float avgX = 0.f;

    for (int x = loi_1.x; x <= loi_2.x; ++x)
    {
        int idx = 3 * (y * imgW + x);
        for (int x_ = -1; x_ <= 1; ++x_)
        {
            for (int y_ = -1; y_ <= 1; ++y_)
            {
                if (data[idx + 3 * ((y_ * imgW) + x_)] == (char)255)
                {
                    cnt++;
                    avgX += x + x_;
                }
            }
        }

        delta += slope;
        if (delta >= 0.5)
        {
            y--;
            delta -= 1.0;
        }
    }

    if (cnt > 0) avgX = avgX / cnt;
    float per = 1.f * cnt / (9 * (loi_2.x - loi_1.x));
    if (cnt > 10 && per < 0.3)
    {
        heatmap[(int)avgX - loi_1.x]++;
    }
    return 0;
}

void Anl_CrossLine::displayHeatmap()
{
    for (auto it = heatmap.begin(); it != heatmap.end(); ++it)
        cout << (*it) << ' ';
    cout << endl;
}

int Anl_CrossLine::computeLanes()
{
    vector<int> lane_div;
    int minZeroPad = 15;
    int cntZ = 0;
    int zeroStart = 0;
    bool firstLane = true;

    lane_div.push_back(loi_1.x);
    for (auto it = heatmap.begin(); it != heatmap.end(); ++it)
    {
        if ((*it) == 0 || (*(it-1) == 0 && *(it+1) == 0)) {
            if (cntZ == 0) {
                zeroStart = it - heatmap.begin();
            }
            cntZ++;
        }
        else 
        {
            if (cntZ > minZeroPad) {
                if (firstLane) firstLane = false;
                else
                {
                    int mean = ((it - heatmap.begin()) - zeroStart) / 2;
                    lane_div.push_back(loi_1.x + zeroStart + mean);
                }
            }
            cntZ = 0;
        }
    }
    lane_div.push_back(loi_2.x);

    // compute corners for each lane
    float a = (loi_2.x - loi_1.x) * 1.f;
    float b = (loi_2.y - loi_1.y) * 1.f;
    float slope = b / a;
    float den = sqrt(pow(a, 2) + pow(b, 2));
    for (auto it = lane_div.begin(); it != lane_div.end(); ++it)
    {
        int y = loi_1.y + slope * ((*it) - loi_1.x);
        int p1_x, p2_x, p1_y, p2_y;
        if (slopeInv != 0)
        {
            p1_x = (*it) - gap * cos(slopeInv * 3.14159265 / 180.0);
            p2_x = (*it) + gap * cos(slopeInv * 3.14159265 / 180.0);
            p1_y = y + gap * sin(slopeInv * 3.14159265 / 180.0);
            p2_y = y - gap * sin(slopeInv * 3.14159265 / 180.0);
        }
        else
        {
            p1_x = (*it) + (gap * b / den);
            p2_x = (*it) - (gap * b / den);
            p1_y = y - (gap * a / den);
            p2_y = y + (gap * a / den);
        }
        lanes.push_back(pair<vec2i, vec2i>{vec2i{p1_x, p1_y}, vec2i{p2_x, p2_y}});
    }


    return 0;
}

void Anl_CrossLine::analyze(char* bg_sub, char* src, int t)
{
    cout << "\n------------------------------------------------------------\n";
    cout << "[frame #" << t << "]  TOTAL CROSS = " << totalCross << endl;
    int numLanes = lanes.size() - 1;
    vector<int> cntIntr(numLanes * 2, 0);

    // if t = 0, initialize crossingStatus
    if (t == 0)
    {
        for (int i = 0; i < numLanes; i++)
        {
            crossingStatus.push_back(0);
        }
    }

    // for each pixel
    for (int y = 0; y < imgH; ++y)
    {
        for (int x = 0; x < imgW; ++x)
        {
            vec2i pt{x, y};
            int idx = 3 * (y * imgW + x);
            for (int l = 0; l < numLanes; ++l)
            {
                if (isPosRight(lanes[l].second, lanes[l].first, pt) &&
                    !isPosRight(lanes[l].second, lanes[l+1].second, pt) &&
                    !isPosRight(lanes[l+1].second, lanes[l+1].first, pt) &&
                    isPosRight(lanes[l].first, lanes[l+1].first, pt))
                {
                    src[idx + l%3] = (char) 100;
                    if (isPosRight(loi_1, loi_2, pt))
                    {
                        src[idx + l%3] = (char) 200;
                        if (bg_sub[idx] == (char)255 ) cntIntr[2 * l]++;
                    }
                    else
                    {
                        if (bg_sub[idx] == (char)255 ) cntIntr[2 * l + 1]++;
                    }
                }
            }
        }
    }

    cout << "Lane Status: ";
    for (int i = 0; i < numLanes; ++i)
    {
        int diff_ = cntIntr[2 * i + 1] - cntIntr[2 * i];
        int sum_ = cntIntr[2 * i] + cntIntr[2 * i + 1];
        int laneSize = abs(lanes[i].second.x - lanes[i].first.x) * \
                       abs(lanes[i+1].second.y - lanes[i].second.y);
        // check for status change
        // only check when the difference is greater then 80%
        float prop = 1.0 * abs(diff_) / laneSize;
        if (prop > 0.4) 
        {
            if (crossingStatus[i] == 0)
            {
                if (diff_ > 0) crossingStatus[i] = 2;
                else crossingStatus[i] = 1;
            } else if (crossingStatus[i] == 1)
            {
                if (diff_ > 0)
                {
                    crossingStatus[i] = 3;
                    totalCross++;
                }
            } else if (crossingStatus[i] == 2)
            {
                if (diff_ < 0)
                {
                    crossingStatus[i] = 4;
                    totalCross++;
                }
            } 
        }
        else
        {
            if (crossingStatus[i] == 3 || crossingStatus[i] == 4)
                crossingStatus[i] = 0;
        }

        // print out
        cout << "     " << i << ": ";
        if (crossingStatus[i] == 0) cout << "empty";
        else if (crossingStatus[i] == 1) cout << "crossing in [R->L]";
        else if (crossingStatus[i] == 2) cout << "crossing in [L->R]";
        else if (crossingStatus[i] == 3) cout << "crossing out [R->L]";
        else if (crossingStatus[i] == 4) cout << "crossing out [L->R]";
    }
}

void Anl_CrossLine::draw_line(vec2i pt1, vec2i pt2, IplImage* img)
{
    uchar r, g, b;
    r = 255; g = 255; b =   0;          // yellow

    float slope = abs(1.0 * (pt2.y - pt1.y) / (pt2.x - pt1.x));

    // assume in color channel
    int y = pt1.y;
    float delta = 0.;
    for (int x = pt1.x; x <= pt2.x; ++x)
    {
        int idx = y * img->widthStep + 3 * x;
        img->imageData[idx] = b;
        img->imageData[idx + 1] = g;
        img->imageData[idx + 2] = r;

        delta += slope;
        if (delta >= 0.5)
        {
            y--;
            delta -= 1.0;
        }
    }
}
