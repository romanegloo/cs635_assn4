/* 
 * Author: Jiho Noh (jiho@cs.uky.edu)
 *
 * This is a my version of the paper "Adaptive background mixture models
 * for real-time tracking" published by Chris Stauffer and W.E.L Grimson (MIT)
 */

#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

typedef struct
{
    float var;    // assume that R, G, B are independent to each other 
                  // and have the same variance
    float muB;
    float muG;
    float muR;
    float weight;
}gd;
typedef unsigned char uchar;

int load_srcImages(int verb);
bool fileExist(string fileName);

//vector<IplImage*> srcData;
//int tStart = 1;
//int frmLen = 200;
//const char* ptnInputImg_A = "dataset/A-Afternoon-Yang-office-clean/A%04d.PNG";
//const char* ptnInputImg_B = "dataset/B-Afternoon-Yang-office-busy/B%04d.PNG";
//const char* ptnInputImg_C = "dataset/shadow_images/%03d.png";
//const char* ptnInputImg = ptnInputImg_A;

class BGD_GMM
{
    public:
    float fAlpha;
    // fAlpha - learning rate. This is used to normalize the weight
    // parameters. If you use T frames to average the background, set this to
    // 1/T.  quote from the paper - "1/alpha defines the time constant which
    // determines the speed at which the distribution's parameters change."
    
    private:
    int nK;  
    // nK - number of Gaussian distributions to keep track
    float fTsig;
    // fTsig - threshold which determines how close it should consider to a
    // matching distribution. The paper defines a pixel value within 2.5
    // standard deviation
    float fSig0;
    // fSig0 - initial standard deviation for new distribution
    float fTbgw;
    // fTbgw - threshold which determines whether the given pixel should be
    // considered as background or not

    int nChannels;
    int nWidthStep;
    int nWidth;
    int nHeight;
    int nSize;         // nWidth * nHeight

    
    gd* mm;             // for mixture of K distributions
    uchar* nGD;         // number of Gaussian distributions created  per pixel

    public:
    BGD_GMM(int w, int h);
    void bgdUpdatePixelGD(char* src, char* bg, char* bg_sub);
};
