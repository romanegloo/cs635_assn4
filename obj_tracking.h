#ifndef __OBJ_TRACKING_H_INCLUDED__
#define __OBJ_TRACKING_H_INCLUDED__
/*
 * Author: Jiho Noh (jiho@cs.uky.edu)
 *
 * Simple motion-based object tracking class 
 */

using namespace cv;
using namespace std;

typedef struct {
    int id;                        // unique id number
    //bb bbox;                       // bounding box, display purpose
    vector<vec2d> centroids;       // history of centroids
    vector<bb> assignedBlob;      // matching blobs
    int age;                       // total frames that obj exists
    int visibleCount;              // number of visible frames
    int invisibleCount;            // number of consecutive invisible frames
    int pos;                       // 0 (unknown), 1 (left), 2 (right) compared
                                   // to the line of interest
} movingObj2;
typedef struct { int accX; int accY; int n;} avgCent;

class ObjTrack
{
    private:
    float Tcost_new;               // threshold of cost to create new track
    int TminSize;                // minimum size of moving object, smaller 
                                   // object will be ignored
    vector<movingObj2> objs;       // all unassigned or assigned tracks
    int totalCross;                // for counting cross
    vec2i loi_1, loi_2;            // cross line
    int lastTrackId;               // last id number
    int kClusters;                 // number of clusters to be estimated in 
                                   // k-means clustering algorithm
    const uchar max_kClusters = 20;  // maximum 20 objects can be recognized


    public:
    ObjTrack(vec2i, vec2i);
    void assignDetections(map<int, bb>);    // assign detections to tracks
    void createNewTracks(map<int, bb>, vector<int>);  
       // create new tracks from unassigned detections
    void updateTracks(map<int, avgCent>);           // update all tracks
    void report(int t, int frmLen, IplImage* src);   // display information

    int isPosRight(vec2i);         // determine current position by its 
                                   // predicted position
    void draw_loi(IplImage*, int color);  // draw the line

    vector<vec2d> estimateNumClusters(IplImage*); 
        // estimate # of clusters for K-means
    int runKmeansClustering(IplImage*, int k, vector<vec2d>& centroids);

};
#endif
